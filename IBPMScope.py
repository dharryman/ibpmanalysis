import pyvisa
import time
from IBPMDataFile import IBPMDataFile

class IBPMScope():
    def __init__(self, scopeVISA = 'TCPIP0::172.23.233.138::hislip0::INSTR'):
        self.rm = pyvisa.ResourceManager()
        print(self.rm.list_resources())
        self.inst = self.rm.open_resource(scopeVISA)
        self.inst.timeout = 60000
        print(self.inst.query("*IDN?"))
        self.lastAction = ""

    # This makes the next saved waveforms save with the suffix 90000 onwards
    def resetSavedWaveformID(self):
        cmd = ":DISK:LOAD \"DHAuxTrigIBPMSaving0.set\""
        self.inst.write(cmd)
        self.lastAction = ""

    # This resumes the running total saved with the "disableSaveonTrigger" method
    def enableSaveOnTrigger(self):
        cmd = ":DISK:LOAD \"DHAuxTrigIBPMSavingRunning.set\""
        self.inst.write(cmd)
        self.lastAction = ""

    # This records the current waveform number and saves it as a running total
    # This makes the next saved waveforms save with the suffix 00000 onwards
    def disableSaveonTrigger(self):
        if self.lastAction != "disableSaveonTrigger":
            cmd = ":DISK:SAVE:SETup \"DHAuxTrigIBPMSavingRunning.set\""
            self.inst.write(cmd)
            time.sleep(3)

        cmd = ":DISK:LOAD \"DHAuxTrigIBPM.set\""
        self.inst.write(cmd)
        self.lastAction = "disableSaveonTrigger"

    # This pulls out the latest wavefrom from the scope and plots it
    def plotLatestWaveform(self):
        cmd = ":DISK:SAVE:WAV ALL,\"currentWF.h5\""
        print(cmd)
        self.inst.write(cmd)

        time.sleep(1)
        #For use on macbook
        # path = "/Volumes/Infiniium/currentWF.h5"

        #for use on windows
        path = "Z:\currentWF.h5"

        ibpm = IBPMDataFile(path)
        ibpm.plotChannels()

    def quickIntegrate(self):
        cmd = ":DISK:SAVE:WAV ALL,\"currentWF.h5\""
        print(cmd)
        self.inst.write(cmd)

        time.sleep(1)

        path = "Z:\currentWF.h5"

        bpmDataFile = IBPMDataFile(path, 1000, 200)

        bpmDataFile.setBunchStartTime("A", 498.)
        bpmDataFile.setBunchStartTime("B", 498.)
        bpmDataFile.setBunchStartTime("C", 498.)
        bpmDataFile.setBunchStartTime("D", 498.)
        bpmDataFile.setBunchLength(5.)
        bpmDataFile.removeChannelOffsets(50)
        xTemp, yTemp = bpmDataFile.getBeamPosition()
        print(xTemp, yTemp)
        iTemp = bpmDataFile.getBeamIntensity()
        print(iTemp)
        bpmDataFile.plotBunches()
