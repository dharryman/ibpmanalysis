import pyvisa
from ftplib import FTP
import matplotlib.pyplot as plt
from IBPMDataFile import IBPMDataFile
import time
#
rm = pyvisa.ResourceManager()
print(rm.list_resources())
# inst = rm.open_resource('TCPIP0::172.23.233.138::hislip0::INSTR')
inst = rm.open_resource('TCPIP0::172.23.233.138::hislip0::INSTR')
inst.timeout = 60000
print(inst.query("*IDN?"))
#cmd = ":DISK:LOAD \"inductiveBPM1bunchAuxTrig.set\""
#cmd = ":DISK:SAVE:SETup \"DHTestSet10101.set\""
# cmd = ":DISK:LOAD \"DHtest.set\""
# cmd = ":DISK:LOAD \"inductiveBPM1bunch.set\""
# cmd = ":DISK:LOAD \"setup3.set\""
# print(cmd)
# print(inst.write(cmd))

# print(inst.query(":DISK:PWD?"))
# # x = inst.read()
# # print(x)

cmd  = ":DISK:SAVE:WAV ALL,\"currentWF.h5\""
print(cmd)
inst.write(cmd)
#
time.sleep(1)
# path = "/Volumes/Infiniium/currentWF.h5"
path = "Z:\currentWF.h5"

ibpm = IBPMDataFile(path)
ibpm.plotChannels()
