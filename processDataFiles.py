from IBPMDataFile import IBPMDataFile
import os
import numpy as np
import matplotlib.pyplot as plt
import csv

# path = "/users/dharryman/desktop/IBPM Data 010519/"
# scopeFiles = os.listdir(path)
# scopeFiles.sort()

scopeFile = r"Z:\DH June2019\testing\WF90070.h5"

x = []
y = []
intensity = []
index = 0
# bpmDataFile = IBPMDataFile(path+scopeFile, 1000, 160)
bpmDataFile = IBPMDataFile(scopeFile, 1000, 200)
bpmDataFile.setBunchStartTime("A", 498.)
bpmDataFile.setBunchStartTime("B", 498.)
bpmDataFile.setBunchStartTime("C", 498.)
bpmDataFile.setBunchStartTime("D", 498.)
bpmDataFile.setBunchLength(3.)
bpmDataFile.removeChannelOffsets(50)
xTemp, yTemp = bpmDataFile.getBeamPosition()
print(xTemp, yTemp)
iTemp = bpmDataFile.getBeamIntensity()
intensity.append(iTemp)
bpmDataFile.plotBunches()
# bpmDataFile.plotChannels()



