import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import h5py

class IBPMDataFile():
    def __init__(self, fileName, captureTime=0, voltageSpan=0, dp=2):
        self.file = h5py.File(fileName, 'r')
        self.bunchLength = 0
        chA = self.file.get("Waveforms/Channel 1/Channel 1Data")
        chB = self.file.get("Waveforms/Channel 2/Channel 2Data")
        chC = self.file.get("Waveforms/Channel 3/Channel 3Data")
        chD = self.file.get("Waveforms/Channel 4/Channel 4Data")
        self.dp = dp
        self._defaultChannelDict = {
            "A": 0.,
            "B": 0.,
            "C": 0.,
            "D": 0.}
        self.channels = {
            "A": np.array(chA),
            "B": np.array(chB),
            "C": np.array(chC),
            "D": np.array(chD)}
        self.channelOffsets = {
            "A": 0.,
            "B": 0.,
            "C": 0.,
            "D": 0.}
        self.bunchStartTimes = {
            "A": 0.,
            "B": 0.,
            "C": 0.,
            "D": 0.}
        self.channelIntegrals = {
            "A": 0.,
            "B": 0.,
            "C": 0.,
            "D": 0.}
        self.bunches = {
            "A": 0.,
            "B": 0.,
            "C": 0.,
            "D": 0.}
        self.bunchTimebases = {
            "A": 0.,
            "B": 0.,
            "C": 0.,
            "D": 0.}
        self.frame = self.file.get("Frame/TheFrame")
        self.timeStamp = self.frame[...]
        self.timeStamp = str(self.timeStamp)
        self.timeStamp = self.timeStamp.split(",")
        self.timeStamp = self.timeStamp[2].split("'")
        self.timeStamp = self.timeStamp[1]
        # As the BPM produces negative signals, the sacle factors are negative to compensate.
        # the next issue is to work out the magnitude of these scaling factors.
        self.xPositionScaleFactor = -13800 # 13.8mm
        self.yPositionScaleFactor = -13800
        self.intensityScaleFactor = -0.4 # 0.4 volts per second
        if captureTime == 0:
            self.timeAxisLabel = "Capture Time (Samples)"
            self.timebase = np.linspace(0, len(chA), len(chA))
        else:
            self.timeAxisLabel = "Capture Time (nS)"
            self.timebase = np.linspace(0, captureTime, len(chA))

        if voltageSpan == 0:
            self.signalAxisLabel = "Signal Amplitude (ADC Counts)"
        else:
            self.signalAxisLabel = "Signal Amplitude (mV)"
            self.channels["A"] = self.channels["A"] * (voltageSpan / 65536.)
            self.channels["B"] = self.channels["B"] * (voltageSpan / 65536.)
            self.channels["C"] = self.channels["C"] * (voltageSpan / 65536.)
            self.channels["D"] = self.channels["D"] * (voltageSpan / 65536.)


    def removeSingleChannelOffset(self, channelKey, timeBeforePulse):
        deadtime = [i for i in self.timebase if i < timeBeforePulse]
        ch = self.channels[channelKey]
        ch = ch[0:len(deadtime)]
        chOffset = np.mean(ch)
        self.channelOffsets[channelKey] = chOffset
        self.channels[channelKey] = self.channels[channelKey] - chOffset

    def removeChannelOffsets(self, timeBeforePulse):
        # Only use this method if the time before pulse is the same for all channels
        # Otherwise use the removeSingleChannelOffset method
        self.removeSingleChannelOffset("A", timeBeforePulse)
        self.removeSingleChannelOffset("B", timeBeforePulse)
        self.removeSingleChannelOffset("C", timeBeforePulse)
        self.removeSingleChannelOffset("D", timeBeforePulse)

    def getBeamPosition(self):
        a = self._processChannel("A")
        b = self._processChannel("B")
        c = self._processChannel("C")
        d = self._processChannel("D")

        x = ((a - c) / (a + c)) * self.xPositionScaleFactor
        y = ((b - d) / (b + d)) * self.yPositionScaleFactor
        return (x, y)

    def getBeamIntensity(self):
        a = self._processChannel("A")
        b = self._processChannel("B")
        c = self._processChannel("C")
        d = self._processChannel("D")
        intensity = self.intensityScaleFactor * (a + b + c + d)
        return (intensity)

    def _processChannel(self, channelKey):
        ch = self.channels[channelKey]
        bunchStartTime = self.bunchStartTimes[channelKey]
        chTemp = []
        timeTemp = []
        for time, channel in zip(self.timebase, ch):
            if time > bunchStartTime:
                timeTemp.append(time)
                chTemp.append(channel)
        ch = chTemp
        integralTime = timeTemp
        dif = integralTime[1] - integralTime[0]
        integralTime = integralTime[0:int(self.bunchLength/dif)]
        ch = ch[0:int(self.bunchLength / dif)]
        self.bunches[channelKey] = ch
        self.bunchTimebases[channelKey] = integralTime
        self.channelIntegrals[channelKey] = np.sum(ch)
        return self.channelIntegrals[channelKey]

    def setBunchLength(self, bunchLength):
        self.bunchLength = bunchLength

    def setBunchStartTime(self, channelKey, bunchStartTime):
        self.bunchStartTimes[channelKey] = bunchStartTime

    def getChannelIntegrals(self):
        a = self.channelIntegrals["A"]
        b = self.channelIntegrals["B"]
        c = self.channelIntegrals["C"]
        d = self.channelIntegrals["D"]

    def plotBunches(self, saveFig = False):
        title = "Bunch Data: "+self.timeStamp
        plt.title(title)
        aLabel, bLabel, cLabel, dLabel = self._channelOffsetStrings()

        plt.plot(self.bunchTimebases["A"], self.bunches["A"], label=aLabel)
        plt.plot(self.bunchTimebases["B"], self.bunches["B"], label=bLabel)
        plt.plot(self.bunchTimebases["C"], self.bunches["C"], label=cLabel)
        plt.plot(self.bunchTimebases["D"], self.bunches["D"], label=dLabel)
        plt.xlabel(self.timeAxisLabel)
        plt.ylabel(self.signalAxisLabel)

        plt.legend()


        if saveFig == True:
            plt.savefig(title+".svg")
        plt.show()

    def plotChannels(self, saveFig=False):
        fig, ax = plt.subplots()
        title = "Full Trace Data: " + self.timeStamp

        aLabel, bLabel, cLabel, dLabel = self._channelOffsetStrings()

        plt.title(title)
        plt.plot(self.timebase, self.channels["A"], label=aLabel)
        plt.plot(self.timebase, self.channels["B"], label=bLabel)
        plt.plot(self.timebase, self.channels["C"], label=cLabel)
        plt.plot(self.timebase, self.channels["D"], label=dLabel)
        plt.xlabel(self.timeAxisLabel)
        plt.ylabel(self.signalAxisLabel)
        plt.legend()
        #plt.ylim([-50,10])
        if saveFig == True:
            plt.savefig(title + ".svg")
        plt.show()

    def _channelOffsetStrings(self):
        if self.channelOffsets != self._defaultChannelDict:
            tmpstrA = "Channel A: " + str(round(self.channelOffsets["A"], self.dp)) + " Offset Removed"
            tmpstrB = "Channel B: " + str(round(self.channelOffsets["B"], self.dp)) + " Offset Removed"
            tmpstrC = "Channel C: " + str(round(self.channelOffsets["C"], self.dp)) + " Offset Removed"
            tmpstrD = "Channel D: " + str(round(self.channelOffsets["D"], self.dp)) + " Offset Removed"
        else:
            tmpstrA = "Channel A"
            tmpstrB = "Channel B"
            tmpstrC = "Channel C"
            tmpstrD = "Channel D"
        return (tmpstrA, tmpstrB, tmpstrC, tmpstrD)
